# YrWeather

An unofficial Flutter plugin for fetching data from yr.no.

*Location and coordinate based forecast is currently only available in Norway. To use it in other countries, you will need to make a custom call.*

You have to visibly credit yr.no to use this data. Please read the [terms of service](https://hjelp.yr.no/hc/en-us/articles/360001946134-Data-access-and-terms-of-service "Terms of service").
The Forecast object will contain creditText and creditUrl. You can use these to credit yr.no.

## Usage
```
import 'package:YrWeather/YrWeather.dart';

*...*

Forecast forecast = await YrForecast.getWeatherDataWithUTMCoordinates(169237.18, 6597716.0);

```

An example where it's used in a FutureBuilder. 
[Project link](https://gitlab.com/martinhodne/weatherapp "Weather test app") 
```
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Yr testapp',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
      appBar: AppBar(
        title: Text("Yr testapp"),
      ),
      body: FutureBuilder<Forecast>(
  future: YrForecast.getWeatherDataWithLocation(language: "no"),
  builder: (context, snapshot) {
    if (snapshot.hasData) {
      List<ForecastData> fcdata = snapshot.data.forecast;
      var format = new DateFormat('dd:MM:yyyy HH:mm');
      return ListView.builder(
        itemCount: fcdata.length,
        itemBuilder: (context, index) => ListTile(
        title: Text(fcdata[index].place),
        subtitle: Text(
        "Type vær: " + fcdata[index].typeOfWeather + 
        "\nTemperatur:" + fcdata[index].temperature.toString() + 
        "\nFra: " + format.format(fcdata[index].dateAndTimeFrom) +
        "\nTil: " + format.format(fcdata[index].dateAndTimeTo) +
        "\nVindretning: " + fcdata[index].windDirection + 
        "\nVindstyrke: " + fcdata[index].windSpeed),
        leading: Image.network(fcdata[index].weatherPictureURL)
        ),
      );
    } else if (snapshot.hasError) {
      return Text("${snapshot.error}");
    }
    return CircularProgressIndicator();
  },
),
    ));
  }
}
```

### Different types of calls
The default language is Norwegian Bokmål. To get the forecast in English, use the parameter `language = "en"` for English or `language = "nn"` for Nynorsk. 

#### Get weather data with the location of the phone
This will currently only work in Norway.
```
static Future<Forecast> getWeatherDataWithLocation({String language = "no", bool isHourByHour = false})
```

#### Get weather data with UTM coordinates
This will currently only work in Norway.
```
static Future<Forecast> getWeatherDataWithUTMCoordinates(double east, double north, int zoneNumber, {String language = "no", bool isHourByHour = false, bool largeArea=false})
```

####  Get weather data with latitude and longitude
This will currently only work in Norway.
```
static Future<Forecast> getWeatherDataWithLatitudeAndLongitude(double latitude, double longitude, {String language = "no", bool isHourByHour = false})
```

#### Get weather data with custom parameters
```
static Future<Forecast> getWeatherDataWithCustomParameters(String country, String county, String municipality, String place, {String language = "no", bool isHourByHour = false})
```

#### Get weather data with a custom URL
```
static Future<Forecast> getWeatherDataWithCustomURL(String url)
```