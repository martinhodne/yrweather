library yr_weather;
import 'Forecast.dart';
import 'package:dio/dio.dart';
import 'package:xml/xml.dart' as xml;
import 'package:location/location.dart';
import 'package:utm/utm.dart';

class YrForecast {

// TODO: Add caching
// TODO: Make the location serach work internationally


/// Will get the latitude and longitude from the GPS and pass it on to the function getWeatherDataWithLatitudeAndLongitude()
/// 
/// Henter lengdegrad og breddegrad fra GPS og sender data videre til funksjonen getWeatherDataWithLatitudeAndLongitude()
static Future<Forecast> getWeatherDataWithLocation({String language = "no", bool isHourByHour = false}) async{
  Location location = new Location();

  bool _serviceEnabled;
  PermissionStatus _permissionGranted;
  LocationData _locationData;
  
  _serviceEnabled = await location.serviceEnabled();
  if(!_serviceEnabled){
    _serviceEnabled = await location.serviceEnabled();
    if(!_serviceEnabled){
      throw "Location data not enabled";
    }
  }

  _permissionGranted = await location.hasPermission();
  if(_permissionGranted == PermissionStatus.DENIED){
    _permissionGranted = await location.requestPermission();
    if(_permissionGranted != PermissionStatus.GRANTED){
      throw "Location data not granted";
    }
  }

  _locationData = await location.getLocation();


return await getWeatherDataWithLatitudeAndLongitude(_locationData.latitude, _locationData.longitude, language: language, isHourByHour: isHourByHour); 
}

/// Converts the latitutude and longitude to UTM coordinates 
/// 
/// Konverterer lengdegrad og breddegrad til UTM koordinater
static Future<Forecast> getWeatherDataWithLatitudeAndLongitude(double latitude, double longitude, {String language = "no", bool isHourByHour = false}) async {
  UtmCoordinate utm = UTM.fromLatLon(lat: latitude, lon: longitude, type: GeodeticSystemType.grs80);
  return await getWeatherDataWithUTMCoordinates(utm.easting, utm.northing, utm.zoneNumber, language: language, isHourByHour: isHourByHour);
}

/// If the developer wants to do a custom forecast, he can do so here. 
/// 
/// Hvis utvikleren ønsker å gjøre sin egen spørring til yr.no kan han gjøre det her
static Future<Forecast> getWeatherDataWithCustomParameters(String country, String county, String municipality, String place, {String language = "no", bool isHourByHour = false}) async{
  _Language lang = _languagePicker(language);
  String url = "https://www.yr.no/" + lang._nameOfTheWordPlace + "/" + country + "/" + county + "/" + municipality + "/" + place + "/" + 
  (isHourByHour ? (lang._isNorwegian ? "varsel_time_for_time.xml" : "forecast_hour_by_hour.xml") : (lang._isNorwegian ? "varsel.xml" : "forecast.xml"));
  return await _getWeatherData(url);
}

/// If the developer wants to do his own custom API call to yr.no, he can do so here.
/// 
/// Dersom utvikleren ønsker å gjøre sin helt egen spørring fra URL til yr.no kan han gjøre det her. 
static Future<Forecast> getWeatherDataWithCustomURL(String url) async{
  return await _getWeatherData(url);
}

/// Will ask Kartverket for what your place is called, then it will parse the data, create an URL and call on _getWeatherData(). 
/// If the function finds that it doesn't have any hits, it will make the search area larger and call on itself. 
/// This will only work for Norway (Svalbard and Jan Mayen not included)
/// 
/// Sender en spørring til Kartverket med UTM koordinater for hva det stedet heter. Funksjonen vil så hente ut data, lage en URL til yr og kalle på _getWeatherData(). 
/// Dersom funksjonen ikke har noen treff vil søkeområdet bli større og kalle på seg selv. 
/// Dette fungerer kun i fastlands-Norge.
static Future<Forecast> getWeatherDataWithUTMCoordinates(double east, double north, int zoneNumber, {String language = "no", bool isHourByHour = false, bool largeArea=false}) async{
  String url;
  String a = 'https://ws.geonorge.no/SKWS3Index/ssr/sok?' +
  'ostLL=' + (east - (largeArea ? 9960:1000)).toString() + 
  '&ostUR=' + (east + (largeArea ? 9960:1000)).toString() + 
  '&nordLL=' + (north - (largeArea ? 9960:1000)).toString() + 
  '&nordUR=' + (north + (largeArea ? 9960:1000)).toString() +
  '&epsgKode=' + "258" + zoneNumber.toString();

  Dio dio = new Dio();
  Response res = await dio.get(a);
  try{
    var doc = xml.parse(res.data);
    
    int numberOfHits = int.parse(doc.findElements("sokRes").map((node) => node.findElements("totaltAntallTreff").first).first.firstChild.toString());
        print("Treff: $numberOfHits");
    if(numberOfHits == 0 && !largeArea)
    {
      print("Øker størrelsen...");
      return await getWeatherDataWithUTMCoordinates(east, north, zoneNumber, language: language, isHourByHour: isHourByHour, largeArea: true);
    }
    else if(numberOfHits == 0 && largeArea){
      Future.error("Didn't find a place");
    } 
    else
    {
    var closestResult = doc.findElements("sokRes").map((node) => node.findElements("stedsnavn").first);

      _Language lang = _languagePicker(language);

    String county = closestResult.map((node) => node.findAllElements("fylkesnavn")).first.first.text.split(" - ").first.replaceAll(" ", "_");

    String municipality = closestResult.map((node) => node.findAllElements("kommunenavn")).first.first.text.split(" - ").first.replaceAll(" ", "_");

    String place = closestResult.map((node) => node.findAllElements("stedsnavn")).first.first.text.split(" - ").first.replaceAll(" ", "_");

    url = "https://www.yr.no/" + lang._nameOfTheWordPlace + "/" + lang._nameOfNorway + "/" + county + "/" + municipality + "/" + place + "/" + 
    (isHourByHour ? (lang._isNorwegian ? "varsel_time_for_time.xml" : "forecast_hour_by_hour.xml") : (lang._isNorwegian ? "varsel.xml" : "forecast.xml"));
    }
  }
  catch(e)
  {
    Future.error("Coordinates not found: $e");
  }
    return _getWeatherData(url);
  }

/// Will send a get request to Yr and parse it into the Forecast object.
///
/// Sender en GET forespørsel til Yr og setter dataen inn i et Forecast objekt
   static Future<Forecast> _getWeatherData(String url) async 
   {
    Forecast forecast;
    List<ForecastData> forecastData = new List<ForecastData>();
    forecast = new Forecast();

      await _fetchData(url).then((v)
      {
      try
      {
      var document = xml.parse(v);
      var credit = document
          .findAllElements('credit')
          .map((node) => node.findElements('link'))
          .first;
      forecast.creditText = credit.first.getAttribute('text');
      forecast.creditUrl = credit.first.getAttribute('url').replaceFirst("www", "m");

      String place = document
          .findAllElements("location")
          .map((node) => node)
          .first
          .findElements("name")
          .map((node) => node)
          .first
          .firstChild
          .toString();

      var a = document
          .findAllElements('tabular')
          .map((node) => node.findElements('time'))
          .first;

      var b;
      int l = a.toList().length;
      for (int i = 0; i < l; i++) 
      {
      b = a.elementAt(i);
      String typeOfWeather = b
          .findElements('symbol')
          .map((node) => node.getAttribute('name'))
          .first
          .toString();


      int temperature = int.parse(b
          .findElements('temperature')
          .map((node) => node)
          .first
          .getAttribute('value')
          .toString());


String weatherPictureURL = "https://symbol.yr.no/grafikk/sym/b38/" + b
          .findElements('symbol')
          .map((node) => node.getAttribute('var'))
          .first.toString() + ".png";

      String dateAndTimeFrom = b.getAttribute('from').toString();
      String dateAndTimeTo = b.getAttribute('to').toString();

      DateTime dtFrom = DateTime.parse(dateAndTimeFrom);
      
      DateTime dtTo = DateTime.parse(dateAndTimeTo);
      
      String windSpeed = b
          .findElements('windSpeed')
          .map((node) => node.getAttribute('name'))
          .first
          .toString();

      String windDirection = b
          .findElements('windDirection')
          .map((node) => node.getAttribute('name'))
          .first.toString();

     double precipitation = double.parse(b
      .findElements('precipitation')
      .map((node) => node.getAttribute('value'))
      .first.toString());

      ForecastData fc = new ForecastData(
        // Place
          place,
          // Type of weather
          // Sunny, cloudy, raining.. etc...
          typeOfWeather,
          // Temperature
          temperature,
          // Picture for showing weather
          weatherPictureURL,
          // 
          //weatherName,
          // Date from
          dtFrom,
          // Date to
          dtTo,
          // Wind direction
          windDirection,
          // Wind speed
          windSpeed,

          precipitation
      );

      forecastData.add(fc);
      }
      }
      catch(e)
      {
        print("Error while parsing XML file: " + e.toString());
        throw e;
      }
      forecast.forecast = forecastData;
            return forecast;
      }).catchError((e)
      {
        throw e;
      });
            return forecast;
      }


/// Simple method to fetch data from an URL
/// 
/// Enkel metode for å hente data fra en URL
static dynamic _fetchData(String url) async 
{

  Response response;

  Dio dio = new Dio();
  response = await dio.get(url).catchError((onError){
    print("Error fetching data from Yr");
    throw Exception;
  });

  return response.data; 
  }

/// Will pick the language for the GET request
/// 
/// Velger språk for forespørselen til yr
static _Language _languagePicker(String language)
{
String nameOfNorway;
String nameOfTheWordPlace;
  bool isNorwegian;
  // Will set the language to English if something invalid is typed in
  switch (language.toLowerCase())
  {
  case "no": 
  nameOfTheWordPlace = "sted";
  isNorwegian = true;
  nameOfNorway = "Norge";
  break;
  case "en": 
  nameOfTheWordPlace = "place";
  isNorwegian = false;
  nameOfNorway = "Norway";
  break;
  case "nn": 
  isNorwegian = true;
  nameOfTheWordPlace = "stad";
  nameOfNorway = "Noreg";
  break;
  default:
  nameOfTheWordPlace = "place";
  isNorwegian = false;
  language = "en";
  nameOfNorway = "Norway";
  break;
  }
  _Language lang = new _Language(nameOfNorway, nameOfTheWordPlace, isNorwegian);
  return lang;
}
}

/// Dumb object because Dart doesn't support multiple return parameters
class _Language
{
String _nameOfNorway;
String _nameOfTheWordPlace;
bool _isNorwegian;

_Language(this._nameOfNorway, this._nameOfTheWordPlace, this._isNorwegian);

}
