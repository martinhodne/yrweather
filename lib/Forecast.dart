  /// Forecast object that contains a list of forecast data, text to credit Yr and a link to the weather page on Yr.
  /// 
  /// Værvarsel(Forecast) objekt som inneholder en liste med værvarseldata, tekst til kreditering og lenke til værvarselsiden til det aktuelle stedet på Yr.
  class Forecast
  {
    List<ForecastData> forecast;
    String creditText;
    String creditUrl;
    
}

/// Forecast object for weather data
/// 
/// Værvarseobjekt for værdata
  class ForecastData 
  {
  String _place;
  String _typeOfWeather;
  int _temperature;
  String _weatherPictureURL;
  DateTime _dateAndTimeFrom;
  DateTime _dateAndTimeTo;
  String _windDirection;
  String _windSpeed;
  //Norsk: Nedbør
  double _precipitation;


  ForecastData(this._place, this._typeOfWeather, this._temperature, this._weatherPictureURL, 
  this._dateAndTimeFrom, this._dateAndTimeTo, this._windDirection, this._windSpeed, this._precipitation);
 
  String get place => _place;
  String get typeOfWeather => _typeOfWeather;
  int get temperature => _temperature;
  String get weatherPictureURL => _weatherPictureURL;
  DateTime get dateAndTimeFrom => _dateAndTimeFrom;
  DateTime get dateAndTimeTo => _dateAndTimeTo;
  String get windDirection => _windDirection;
  String get windSpeed => _windSpeed;
  double get precipitation => _precipitation;
  }